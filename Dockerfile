FROM node:12-alpine

WORKDIR /usr/app

COPY ./apptest/package*.json ./
RUN npm install
COPY ./apptest .
RUN npm test

EXPOSE 8080

CMD [ "npm", "start" ]