const { getScore, fetchSomeData } = require("../app/app");

it("should get A", () => {
  const result = getScore(51);
  // Assertion
  expect(result).toEqual("A");
});

it("should get F", () => {
  expect(getScore(1)).toEqual("F");
  expect(getScore(49)).toEqual("F");
});

it("should works with async", async () => {
  const response = await fetchSomeData();
  expect(response).toEqual("success");
});
