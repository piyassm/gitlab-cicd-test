const http = require("http");
const fs = require("fs");
const { getScore, fetchSomeData } = require("../app/app");

http
  .createServer(function (req, res) {
    let mtStream;
    if (req.url === "/home" || req.url === "/") {
      res.writeHead(200, { "Content-Type": "text/html" });
      mtStream = fs.createReadStream(__dirname + "/" + "index.html");
    } else if (req.url === "/facebook") {
      res.writeHead(200, { "Content-Type": "text/html" });
      mtStream = fs.createReadStream(__dirname + "/" + "facebook.html");
    } else if (req.url === "/youtube") {
      res.writeHead(200, { "Content-Type": "text/html" });
      mtStream = fs.createReadStream(__dirname + "/" + "youtube.html");
    } else {
      res.writeHead(404, { "Content-Type": "text/html" });
      mtStream = fs.createReadStream(__dirname + "/" + "notfound.html");
    }
    mtStream.pipe(res);
  })
  .listen(3001);
