exports.getScore = (score) => {
  if (score < 50) {
    return "F";
  }
  return "A";
};

exports.fetchSomeData = () => new Promise((resolve) => resolve("success"));
