docker login registry.gitlab.com
\
docker build -t registry.gitlab.com/mamaiank/gitlab-cicd-test .
\
docker push registry.gitlab.com/mamaiank/gitlab-cicd-test
\
docker pull registry.gitlab.com/mamaiank/gitlab-cicd-test
\
docker run -d -p 8080:80 registry.gitlab.com/mamaiank/gitlab-cicd-test